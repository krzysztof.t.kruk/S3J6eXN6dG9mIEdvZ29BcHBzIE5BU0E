module gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E

go 1.18

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
