FROM golang:1.18-alpine

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o /url-collector

EXPOSE 8080

CMD [ "/url-collector" ]