# API Testing

These tests use [hurl](https://hurl.dev/) to verify API responses and test application end to end.

## Installation
In order to install on mac run:
```shell
brew install hurl
```

## Run
Before running tests, please run application on port `8080`

In order to run all tests at once, run:
```shell
hurl --summary five_days_urls.hurl invalid_params.hurl
```