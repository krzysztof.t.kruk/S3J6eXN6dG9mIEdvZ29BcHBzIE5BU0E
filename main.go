package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/client"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/routing"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/service"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/variable"
	"golang.org/x/sync/semaphore"
)

func main() {
	log.Println("Starting applicaiton...")
	startApp()
}

func startApp() {
	variable.ReadEnv()

	hc := &http.Client{}
	cl := &client.Nasa{Client: hc}
	s := &service.UrlCollector{Client: cl}
	routing.Service = s

	sem := semaphore.NewWeighted(variable.MaxConcurrentRequests)
	service.Semaphore = sem

	r := mux.NewRouter()
	routing.AddRoutes(r)
	http.ListenAndServe(":"+variable.Port, r)
}
