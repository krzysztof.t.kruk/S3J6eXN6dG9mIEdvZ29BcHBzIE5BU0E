package variable

import (
	"log"
	"os"
	"strconv"
)

const DateFormat = "2006-02-01"

var (
	ApiKey                string = "DEMO_KEY"
	MaxConcurrentRequests int64  = 5
	Port                  string = "8080"
)

func ReadEnv() {
	ak := os.Getenv("API_KEY")
	if ak != "" {
		ApiKey = ak
	}

	mcr := os.Getenv("CONCURRENT_REQUESTS")
	i64, err := strconv.ParseInt(mcr, 10, 64)
	if err != nil {
		log.Printf("Error while reading CONCURRENT_REQUESTS, returning to its default value %d, error: %s \n", MaxConcurrentRequests, err.Error())
	}
	if i64 != 0 {
		MaxConcurrentRequests = i64
	}

	p := os.Getenv("PORT")
	if p != "" {
		Port = p
	}
}
