package client_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/client"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/mocks"
)

func TestGetUrlForGivenDay_returnsUrlFromResponse(t *testing.T) {
	//given
	c := client.Nasa{Client: &mocks.HttpClientMock{}}
	expected := "https://apod.nasa.gov/apod/image/1912/TaurusAbolfath1024.jpg"
	json := `{
		"copyright": "Amir H. Abolfath",
		"date": "2019-12-06",
		"explanation": "Lorem ipsum",
		"hdurl": "https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
		"media_type": "image",
		"service_version": "v1",
		"title": "Pleiades to Hyades",
		"url": "%s"
	  }`

	res := fmt.Sprintf(json, expected)
	r := ioutil.NopCloser(strings.NewReader(res))

	mocks.DoFunc = func(req *http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	//when
	actual, _ := c.GetUrlForGivenDay("2019-12-06")

	//then
	assert.Equal(t, expected, actual, "Urls should be the same")
}
