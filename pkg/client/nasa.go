package client

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/variable"
)

const (
	nasaUrl = "https://api.nasa.gov/planetary/apod"
)

type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type Nasa struct {
	Client HTTPClient
}

type response struct {
	Url string `json:"url"`
}

type TooManyRequestsError struct{}

func (e TooManyRequestsError) Error() string {
	return "Too many requests"
}

func (c *Nasa) GetUrlForGivenDay(date string) (string, error) {
	req, err := createRequestWithParams(date)
	if err != nil {
		return "", err
	}

	res, err := c.Client.Do(req)

	defer res.Body.Close()

	if res.StatusCode == 429 {
		return "", TooManyRequestsError{}
	}

	if res.StatusCode != 200 {
		return "", errors.New("Incorrect response code for day: " + date + " code: " + res.Status)
	}

	if err != nil {
		return "", err
	}

	var j response
	err = json.NewDecoder(res.Body).Decode(&j)

	if err != nil {
		return "", err
	}

	return j.Url, nil
}

func createRequestWithParams(date string) (*http.Request, error) {
	req, err := http.NewRequest(http.MethodGet, nasaUrl, nil)
	if err != nil {
		return nil, err
	}
	q := req.URL.Query()
	q.Add("api_key", variable.ApiKey)
	q.Add("date", date)
	req.URL.RawQuery = q.Encode()
	return req, nil
}
