package validate

import (
	"errors"
	"time"
)

func Dates(from time.Time, to time.Time) error {

	if from.After(to) {
		return errors.New(`"from" cannot be later than "to"`)
	}

	if to.After(time.Now()) {
		return errors.New(`"to" cannot be a future date`)
	}

	return nil
}
