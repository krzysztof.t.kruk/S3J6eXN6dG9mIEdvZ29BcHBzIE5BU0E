package routing

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/client"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/routing/validate"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/service"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/variable"
)

var Service *service.UrlCollector

type response struct {
	Urls []string `json:"urls"`
}

type errorResponse struct {
	ErrorMessage string `json:"error"`
}

func AddRoutes(r *mux.Router) {
	r.HandleFunc("/pictures", HandleGetPictures).Methods("GET")
}

func HandleGetPictures(rw http.ResponseWriter, r *http.Request) {
	log.Println(`Incoming request ` + r.RequestURI)

	rw.Header().Add("Content-Type", "application/json")
	from := r.URL.Query().Get("from")
	to := r.URL.Query().Get("to")

	fromT, err := time.Parse(variable.DateFormat, from)
	if err != nil {
		m := fmt.Sprintf(`Incorrect value "from": %s`, err.Error())
		writeErrorResponse(rw, m, http.StatusBadRequest)
		return
	}
	toT, err := time.Parse(variable.DateFormat, to)
	if err != nil {
		m := fmt.Sprintf(`Incorrect value "to": %s`, err.Error())
		writeErrorResponse(rw, m, http.StatusBadRequest)
		return
	}

	err = validate.Dates(fromT, toT)
	if err != nil {
		writeErrorResponse(rw, err.Error(), http.StatusBadRequest)
		return
	}

	urls, err := Service.GetUrlsForDays(context.Background(), fromT, toT)

	if err != nil {
		writeAppropriateErrorResponse(rw, err)
	}

	json.NewEncoder(rw).Encode(&response{urls})
}

func writeErrorResponse(rw http.ResponseWriter, mes string, code int) {
	log.Printf("Error response, code: [%d], message: %s", code, mes)
	rw.WriteHeader(code)
	json.NewEncoder(rw).Encode(&errorResponse{mes})
}

func writeAppropriateErrorResponse(rw http.ResponseWriter, err error) {
	switch err.(type) {
	case client.TooManyRequestsError:
		writeErrorResponse(rw, err.Error(), http.StatusTooManyRequests)
		return
	default:
		writeErrorResponse(rw, err.Error(), http.StatusInternalServerError)
		return
	}
}
