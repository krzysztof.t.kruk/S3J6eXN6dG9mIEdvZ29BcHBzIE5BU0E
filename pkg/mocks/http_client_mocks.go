package mocks

import "net/http"

var (
	DoFunc func(req *http.Request) (*http.Response, error)
)

type HttpClientMock struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (hc *HttpClientMock) Do(req *http.Request) (*http.Response, error) {
	return DoFunc(req)
}
