package mocks

var (
	GetUrlForGivenDayFunc func(date string) (string, error)
)

type NasaClientMock struct {
	GetUrlForGivenDayFunc func(date string) (string, error)
}

func (nc *NasaClientMock) GetUrlForGivenDay(date string) (string, error) {
	return GetUrlForGivenDayFunc(date)
}
