package service_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/mocks"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/service"
	"golang.org/x/sync/semaphore"
)

const (
	format = "2006-02-01"
	url    = "https://apod.nasa.gov/apod/image/1912/TaurusAbolfath1024.jpg"
)

func init() {
	sem := semaphore.NewWeighted(5)
	service.Semaphore = sem
}

func TestGetUrlsForDays_shouldReturnListOfUrls(t *testing.T) {
	//given
	from, _ := time.Parse(format, "2022-12-06")
	to, _ := time.Parse(format, "2022-14-06")

	expected := []string{url, url, url}

	cl := &mocks.NasaClientMock{}
	mocks.GetUrlForGivenDayFunc = func(date string) (string, error) { return url, nil }
	s := service.UrlCollector{Client: cl}

	//when
	actual, err := s.GetUrlsForDays(context.Background(), from, to)

	//then
	if err != nil {
		t.Errorf("Unexpected error: %d", err)
	}

	assert.ElementsMatch(t, expected, actual, "Urls returned should match expected")
}

func TestGetUrlsForDays_shouldReturnAsManyUrlsAsTheNumberOfDaysBetween(t *testing.T) {
	//given
	from, _ := time.Parse(format, "2022-12-06")
	to, _ := time.Parse(format, "2022-14-06")

	cl := &mocks.NasaClientMock{}
	mocks.GetUrlForGivenDayFunc = func(date string) (string, error) { return url, nil }
	s := service.UrlCollector{Client: cl}

	//when
	actual, err := s.GetUrlsForDays(context.Background(), from, to)

	//then
	if err != nil {
		t.Errorf("Unexpected error: %d", err)
	}

	assert.Equal(t, 3, len(actual), "There should be 3 urls returned")
}

func TestGetUrlsForDays_shouldReturnErrorReturnedFromClient(t *testing.T) {
	//given
	from, _ := time.Parse(format, "2022-12-06")
	to, _ := time.Parse(format, "2022-14-06")
	expected := "expected error"

	cl := &mocks.NasaClientMock{}
	mocks.GetUrlForGivenDayFunc = func(date string) (string, error) { return "", errors.New(expected) }
	s := service.UrlCollector{Client: cl}

	//when
	actual, err := s.GetUrlsForDays(context.Background(), from, to)

	//then
	assert.Equal(t, 0, len(actual), "Should return 0 elements")
	assert.Equalf(t, expected, err.Error(), "Error should equal: ", expected)
}
