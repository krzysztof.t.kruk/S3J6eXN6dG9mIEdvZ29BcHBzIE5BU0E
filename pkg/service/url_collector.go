package service

import (
	"context"
	"time"

	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/service/days"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/variable"
	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/semaphore"
)

type PictureClient interface {
	GetUrlForGivenDay(date string) (string, error)
}

type UrlCollector struct {
	Client PictureClient
}

var Semaphore *semaphore.Weighted

func (uc *UrlCollector) GetUrlsForDays(ctx context.Context, from time.Time, to time.Time) ([]string, error) {
	ds := days.GetAllBetween(from, to)
	g, ctx := errgroup.WithContext(ctx)
	c := make(chan string, len(ds))

	for _, d := range ds {
		Semaphore.Acquire(ctx, 1)
		func(d time.Time) {
			g.Go(func() error {
				return callEndpoint(uc, d, c)
			})
		}(d)
	}

	err := g.Wait()
	if err != nil {
		return nil, err
	}

	return collectUrls(c), nil
}

func collectUrls(c chan string) []string {
	close(c)
	urls := make([]string, 0)
	for s := range c {
		urls = append(urls, s)
	}
	return urls
}

func callEndpoint(uc *UrlCollector, d time.Time, c chan string) error {
	defer Semaphore.Release(1)
	r, err := uc.Client.GetUrlForGivenDay(d.Format(variable.DateFormat))
	if err != nil {
		return err
	}
	c <- r
	return nil
}
