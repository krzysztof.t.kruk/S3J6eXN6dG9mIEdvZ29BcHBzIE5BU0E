package days_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/krzysztof.t.kruk/S3J6eXN6dG9mIEdvZ29BcHBzIE5BU0E/pkg/service/days"
)

func TestGetAllBetween(t *testing.T) {
	//given
	from, _ := time.Parse("02-01-2006", "20-01-1994")
	one, _ := time.Parse("02-01-2006", "21-01-1994")
	two, _ := time.Parse("02-01-2006", "22-01-1994")
	to, _ := time.Parse("02-01-2006", "23-01-1994")

	expected := make([]time.Time, 0)
	expected = append(expected, from)
	expected = append(expected, one)
	expected = append(expected, two)
	expected = append(expected, to)

	// when
	actual := days.GetAllBetween(from, to)

	//then
	assert.ElementsMatch(t, expected, actual, "Elements should be the same")
}
