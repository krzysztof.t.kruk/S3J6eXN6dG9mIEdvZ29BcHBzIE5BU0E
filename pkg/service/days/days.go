package days

import "time"

func GetAllBetween(from time.Time, to time.Time) []time.Time {
	days := make([]time.Time, 0)
	days = append(days, from)
	for days[len(days)-1] != to {
		y, m, d := days[len(days)-1].Date()
		nd := time.Date(y, m, d+1, 0, 0, 0, 0, time.UTC)
		days = append(days, nd)
	}
	return days
}
