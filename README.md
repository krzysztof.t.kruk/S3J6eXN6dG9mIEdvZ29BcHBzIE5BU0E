# url-collector

This is a solution to the [recruitment task](https://drive.google.com/file/d/1nR2q2-IqGPOCxDRSQf7F6-Cc0lp-b7uh/view)

## Run with docker
In order to build and run the docker container use commands:

```shell
docker build --tag url-collector .

docker run -p 8080:8080 url-collector
```

In order to set environment variables, please modify `.env` file and run:

```shell
docker run --env-file ./.env -p 8080:8080 url-collector
```

## Hurl testing

You can find [hurl](https://hurl.dev/) tests in directory `./hurl`